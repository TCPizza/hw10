#include <map> 
#include <iterator> 
#include <iostream>
#include<stdlib.h>
#include <string>
#include <sstream>
#include "Item.h"
#include "Customer.h"
#define SIZE 10
#define DEFAULTOPTION 10

static map<string, Customer> abcCustomers;

static Item baseList[SIZE] =//all the available items
{ 
        Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)
};


void AddItems(string newName) {
	int ItemBuyOption = DEFAULTOPTION;
	while (ItemBuyOption != 0) {
		cout << "The items you can buy are: (0 to exit)\n\n";
		for (size_t i = 0; i < SIZE; i++)//printing all the items
		{
			cout << "price: " << baseList[i].GetUnitPrice() << "  "
				<< i + 1 << "  " << baseList[i].GetName() << "\n\n";
		}
		cin >> ItemBuyOption;

		if (1 <= ItemBuyOption && ItemBuyOption <= SIZE) 
		{
			map<string, Customer>::iterator CastumerIter = abcCustomers.find(newName);//adding the custumer name
			CastumerIter->second.addItem(baseList[ItemBuyOption - 1]);//adding the item
		}
	}
}

void RemoveItems(string ClientName) {
	int ItemDeleteOption = DEFAULTOPTION;
	while (ItemDeleteOption != 0) {
		cout << "The items you can remove are: (0 to exit)\n\n";
		Customer cus = abcCustomers.find(ClientName)->second;
		set<Item> _items = cus.GetItems();
		int i = 0;

		set<Item>::iterator itemsIter = _items.begin();

		while (itemsIter != _items.end())//printing the products
		{
			cout << endl << i + 1 << "\nname: " << (*itemsIter).GetName() << "\nprice: " << (*itemsIter).GetUnitPrice() <<
				"\ncount: " << (*itemsIter).GetCount() << "\nSerialNumber: " << (*itemsIter).GetSerialNumber() << endl;
			itemsIter++;
			i++;
		}

		cin >> ItemDeleteOption;
		if (ItemDeleteOption >= 1 && ItemDeleteOption <= _items.size())
		{
			itemsIter = _items.begin();
			int i = 1;
			//get item by option
			while (itemsIter != _items.end() && i < ItemDeleteOption)
			{
				itemsIter++;
				i++;
			}
			Item itemsToDelete = *itemsIter;
			map<string, Customer>::iterator CastumerIter = abcCustomers.find(ClientName);
			CastumerIter->second.removeItem(itemsToDelete);
		}
	}
}

int main() {
	while (true)
	{
		cout << "Welcome to MagshiMart!\n";
		cout << "1.      to sign as customer and buy items\n";
		cout << "2.      to uptade existing customer's items\n";
		cout << "3.      to print the customer who pays the most\n";
		cout << "4.      to exit\n\n";
		int MainOption;
		cin >> MainOption;
		string ClientName;
		map<string, Customer>::iterator CustomerIter;
		double max = 0;
		switch (MainOption)
		{
		case 1:
			cout << "Enter Name\n";
			getline(cin, ClientName);//clear buffer;
			getline(cin, ClientName);
			if (abcCustomers.find(ClientName) == abcCustomers.end())
			{
				abcCustomers.insert(pair<string, Customer>(ClientName, Customer(ClientName)));
				AddItems(ClientName);
			}
			else 
			{
				cout << "The name Exist\n";
			}
			break;
		case 2:
			cout << "Enter Name\n\n";
			getline(cin, ClientName);//clear buffer;
			getline(cin, ClientName);
			if (abcCustomers.find(ClientName) != abcCustomers.end())//cheking if customer exists
			{
				cout << "1.      Add items\n";
				cout << "2.      Remove items\n";
				cout << "3.      Back to menu\n\n";

				int SubTwoCommandOption;
				cin >> SubTwoCommandOption;
				switch (SubTwoCommandOption)
				{
				case 1:
					AddItems(ClientName);
					break;
				case 2:
					RemoveItems(ClientName);
					break;
				case 3:
				default:
					break;
				}
			}
			else
			{
				cout << "this name not exist\n\n";
			}
			break;
		case 3:
			max = 0;
			CustomerIter = abcCustomers.begin();
			while (CustomerIter != abcCustomers.end()) 
			{
				if (CustomerIter->second.totalSum() > max)
				{
					max = CustomerIter->second.totalSum();
					ClientName = CustomerIter->second.GetName();
				}
				CustomerIter++;
			}
			cout << "Client name: " << ClientName << "\nPaid : " << max << "\n\n";
			break;
		case 4:
		default:
			return 0;
			break;
		}
	}
	return 0;
}
