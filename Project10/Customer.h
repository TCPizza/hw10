#pragma once

#include <set> 
#include <iterator> 
#include <iostream>
#include<stdlib.h>
#include "Item.h"

using namespace std;
class Customer {

public:
	Customer();
	Customer(const Customer&);
	Customer(string name, set<Item> items);
	Customer(string name);
	~Customer();

	string GetName();
	void SetName(string);
	set<Item> GetItems();
	void SetItems(set<Item>);



	double totalSum();
	void addItem(Item);
	void removeItem(Item);
private:
	string _name;
	set<Item> _items;
};
